import json
from vosk import Model, KaldiRecognizer
import pyaudio
import pyttsx3

model = Model(lang="cn")
recognizer = KaldiRecognizer(model, 16000)
mic = pyaudio.PyAudio()

# mac os m1
# pip3 install pyobjc==9.0.1
engine = pyttsx3.init()

listening = False


def get_command():
    listening = True
    stream = mic.open(
        format=pyaudio.paInt16,
        channels=1,
        rate=16000,
        input=True,
        frames_per_buffer=8000,
    )
    while listening:
        try:
            data = stream.read(4096)
            if recognizer.AcceptWaveform(data):
                response = recognizer.Result()
                response = json.loads(response)["text"]
                listening = False
                stream.close()
                return response
        except OSError:
            pass


while True:
    print("Waiting for command...")
    command = get_command()
    if command == "":
        pass
    if command != "":
        print(command, "----")
        engine.say(command)
        engine.runAndWait()
